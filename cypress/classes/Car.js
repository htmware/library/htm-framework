import Model from '../../src/model/Model';

export default class Car extends Model {
    tableName = 'car';

    fillable = ['id', 'tires', 'windows'];

    getParams = ['start_date', 'end_date'];

    start_date = undefined;
    end_date = undefined;
}

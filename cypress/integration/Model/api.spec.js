const Car = require('../../classes/Car');
const cars = require('../../data/cars.json');

describe('Test if api calls are executed correctly', () => {
    context('Test the laravel standard resource api calls', () => {
        it('Test the index api call', () => {
            let carsFetched = [];

            cy.intercept({ method: 'GET', url: '/api/car' },
                { data: cars }
            ).as('getCars').then(async () => {
                carsFetched = await Car.all();
                expect(carsFetched.length).to.equal(3);
                expect(carsFetched[0].id).to.eq(1);
                expect(carsFetched[0].windows).to.eq(4);
            })
        });

        it('Test the save api call', () => {
            let car = new Car(cars[0]);
            car.id = undefined;

            cy.intercept({ method: 'POST', url: '/api/car' },
                { data: cars[0] }
            ).as('getCars').then(async () => {
                await car.save();
                expect(car.id).to.equal(1);
            })
        });

        it('Test the update api call', () => {
            let car = new Car(cars[0]);
            car.windows = 8;

            cy.intercept({ method: 'PATCH', url: '/api/car/' + car.id },
                { data: cars[0] }
            ).as('getCars').then(async () => {
                await car.update();
                expect(car.id).to.equal(1);
                expect(car.windows).to.equal(8);
            });
        });

        it('Test the delete api call', () => {
            let car = new Car(cars[0]);

            cy.intercept({ method: 'DELETE', url: '/api/car/' + car.id },
                { data: cars[0] }
            ).as('getCars').then(async () => {
                await car.delete()
                expect(car.id).to.equal(undefined);
            })
        });

        it('Test the get fillable on api call', () => {
            cy.intercept({ method: 'GET', url: '/api/car?start_date=2012&end_date=2014' },
                { data: cars }
            ).as('getCars').then(async () => {
                await Car.all("", { start_date: "2012", end_date: "2014" });
            })

            cy.intercept({ method: 'GET', url: '/api/car?start_date=2013&end_date=2015' },
                { data: cars }
            ).as('getCars').then(async () => {
                let car = new Car();
                car.end_date = "2015";
                car.start_date = "2013";
                await car.get();
            })

            cy.intercept({ method: 'PATCH', url: '/api/car/1?start_date=2013&end_date=2015' },
                { data: cars }
            ).as('getCars').then(async () => {
                let car = new Car();
                car.id = 1;
                car.end_date = "2015";
                car.start_date = "2013";
                await car.update();
            })

        });
    });
});

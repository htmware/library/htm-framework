const Model = require('./model/Model');
const Api = require('./api/Api');
const Request = require('./api/Request');

module.exports = {
    Model,
    Api,
    Request
}

import { LoremIpsum } from "lorem-ipsum";

export default class Factory {
    fillable = [];
    amount = 1;
    increment = 1;
    model = undefined;

    constructor(model) {
        this.fillable = model.fillable
        this.model = model;
    }


    create() {
        let models = [];
        let model = new this.model.constructor();
        console.log(model);
        for (const value of this.fillable) {
            let valueSplittedTop = value.split(':');
            let fields = valueSplittedTop[1].split(',');

            switch (fields[0]) {
                case 'increment':
                    model[valueSplittedTop[0]] = this.increment;
                    this.increment ++;
                    break;
                case 'string':
                    model[valueSplittedTop[0]] = this.amount;
                    break;
                case 'integerBetween':
                    break;
                default:
                    model[fields[0]] = fields[1];
                    break;
            }
        }

        return models;
    }

    count(amount = 1) {
        this.amount = amount;
        return this;
    }
}

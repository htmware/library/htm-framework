import Api from '../api/Api';
import { urlGenerator } from '../helper/UrlHelper';

export default class Model extends Api {
    errors = [];
    fillable = [];
    getParams = [];

    tableName = '';
    tableNameChanged = "";

    constructor(modelData = undefined) {
        super();
        if (modelData !== undefined) {
            for (const dataKey in modelData) {
                if (modelData[dataKey]) {
                    this[dataKey] = modelData[dataKey];
                }
            }
        }

    }

    fill(data) {
        for (const dataKey of this.fillable) {
            if (data[dataKey]) {
                this[dataKey] = data[dataKey];
            }
        }
    }

    storeLocal() {
        let data = {};
        for (const dataKey of this.fillable) {
            data[dataKey] = this[dataKey];
        }
        localStorage.setItem(this.constructor.name.toLowerCase(), JSON.stringify(data));
    }

    getLocal() {
        return JSON.parse(localStorage.getItem(this.constructor.name.toLowerCase()));
    }

    post(url, data) {
        return new Promise((resolve, reject) => {
            this.errors = [];
            super.post(url, data).then((response) => {
                resolve(response);
            })
                .catch((errors) => {
                    this.parseErrors(errors);
                    reject(errors);
                });
        });
    }

    get(url = "") {
        return new Promise((resolve) => {
            super.get(urlGenerator(url, this)).then((response) => {
                resolve(response);
            });
        });
    }

    static get(url = "") {
        return new Promise((resolve) => {
            super.get(urlGenerator(url, new this())).then((response) => {
                resolve(response);
            });
        });
    }

    static all(url = "", getParams = {}) {
        return new Promise((resolve) => {
            let models = [];
            let tmpModel = new this();
            for (let key in getParams) {
                tmpModel[key] = getParams[key];
            }
            super.get(urlGenerator(url, tmpModel)).then((response) => {
                response.data.data.forEach((modelData) => {
                    let newModel = new this();
                    newModel.fill(modelData);
                    models.push(newModel);
                });
                resolve(models)
                return models;
            });
        });
    }

    save(url = "") {
        return new Promise((resolve, reject) => {
            this.errors = [];

            super.post(urlGenerator(url, this), this.parseData()).then((response) => {
                this.id = response.data.data.id;
                resolve(response);
            })
            .catch((errors) => {
                this.parseErrors(errors);
                reject(errors);
            });
        });
    }

    update(url = "") {
        return new Promise((resolve, reject) => {
            this.errors = [];
            let data = this.parseData();
            // convert dataform to object
            let object = {};
            data.forEach((value, key) => object[key] = value);
            super.patch(urlGenerator(url, this, '/' + this.id), object).then((response) => {
                this.id = response.data.data.id;
                resolve(response);
            })
                .catch((errors) => {
                    this.parseErrors(errors);
                    reject(errors);
                });
        });
    }

    delete(url = "") {
        return new Promise((resolve, reject) => {
            this.errors = [];
            super.delete(urlGenerator(url, this, '/' + this.id)).then((response) => {
                this.id = undefined;
                resolve(response);
            })
                .catch((errors) => {
                    this.parseErrors(errors);
                    reject(errors);
                });
        });
    }

    // Original suggestion belongsTo
    // belongsTo(classObject) {
    //     if (!this[classObject.constructor.tableName + "_id"]) {
    //         console.warn(`${classObject.constructor.tableName}_id is not set in model: ${this.constructor.tableName}`);
    //         return;
    //     }
    //     classObject.id = this[classObject.constructor.tableName + "_id"];
    //     return classObject;
    // }

    belongsTo(classObject) {
        let newObject = new classObject();

        if (!this[newObject.tableName + "_id"]) {
            console.warn(`${newObject.tableName}_id is not set in model: ${this.tableName}`);
            return;
        }

        this.tableNameChanged = this.tableName;
        this.tableName = `${newObject.tableName}/${this[newObject.tableName + "_id"]}/${this.tableName}`
        return this;
    }

    parseData() {
        let formData = new FormData();
        for (const formDataKey of this.fillable) {
            if (this[formDataKey] !== undefined || this[formDataKey] !== null) {
                formData.append(formDataKey, this[formDataKey]);
            }
        }
        return formData;
    }

    parseErrors(errors) {
        this.hasErrors = true;
        this.loading = false;
        for (const errorsKey in errors.response.data.errors) {
            if (errorsKey.match('([.][0-9]{1,2})$')) {
                let name = errorsKey.split('.');
                if (this.errors[name[0]] === undefined || this.errors[name[0]] === null) {
                    this.errors[name[0]] = [];
                }
                this.errors[name[0]].push(errors.response.data.errors[errorsKey]);
            } else {

                this.errors[errorsKey] = errors.response.data.errors[errorsKey];
            }
        }
    }

}

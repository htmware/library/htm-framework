import Api from './Api';

export default class Request extends Api {
    errors = [];

    fillable = [];

    sendRequest(url, method = 'POST') {
        this.errors = [];
        switch (method) {
            case 'POST':
                return new Promise(((resolve, reject) => {
                    this.post(url, this.parseData()).then((response) => {
                        resolve(response);
                    }).catch((error) => {
                        this.parseErrors(error)
                        reject(error);
                    });
                }));
            case 'PATCH':
                return new Promise(((resolve, reject) => {
                    this.patch(url, this.parseData()).then((response) => {
                        resolve(response);
                    }).catch((error) => {
                        this.parseErrors(error)
                        reject(error);
                    });
                }));
            default:
                console.error('Non supported method' + method + ' used available are: POST, PATCH');
        }
    }

    parseData() {
        let formData = new FormData();
        for (const formDataKey of this.fillable) {
            if (this[formDataKey] !== undefined || this[formDataKey] !== null) {
                formData.append(formDataKey, this[formDataKey]);
            }
        }
        return formData;
    }

    parseErrors(errors) {
        this.hasErrors = true;
        this.loading = false;
        for (const errorsKey in errors.response.data.errors) {
            if (errorsKey.match('([.][0-9]{1,2})$')) {
                let name = errorsKey.split('.');
                if (this.errors[name[0]] === undefined || this.errors[name[0]] === null) {
                    this.errors[name[0]] = [];
                }
                this.errors[name[0]].push(errors.response.data.errors[errorsKey]);
            }else {
                this.errors[errorsKey] = errors.response.data.errors[errorsKey];
            }
        }
    }
}

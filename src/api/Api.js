import axios from 'axios';

export default class Api {
    constructor() {
        if (process.env.MIX_API_URL === undefined) {
            if (process.env.VUE_APP_API_URL === undefined) {
                let getUrl = window.location;
                axios.defaults.baseURL = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[0] + 'api'
            }else {
                axios.defaults.baseURL = process.env.VUE_APP_API_URL;
            }
        }else {
            axios.defaults.baseURL = process.env.MIX_API_URL;
        }
    }

    getToken() {
        if (localStorage.getItem('token') === undefined || localStorage.getItem('token') === null) {
            return ""
        }
        return JSON.parse(localStorage.getItem('token'));
    }

    post(url, data) {
        return axios.post(url, data, {
            headers: {
                Authorization: 'Bearer ' + this.getToken()
            }
        });
    }

    patch(url, data) {
        return axios.patch(url, data, {
            headers: {
                Authorization: 'Bearer ' + this.getToken()
            }
        });
    }

    delete(url, data) {
        return axios.delete(url, {
            headers: {
                Authorization: 'Bearer ' + this.getToken()
            }
        });
    }

    static get(url = '') {
        return axios.get(url.toLowerCase(), {
            headers: {
                Authorization: 'Bearer ' + new Api().getToken()
            }
        });
    }

    get(url = '') {
        return axios.get(url.toLowerCase(), {
            headers: {
                Authorization: 'Bearer ' + this.getToken()
            }
        });
    }

}

let urlGenerator = function(url, model, routeAffix = "") {
    let path = url;
    if (path.length === 0) {
        path = model.tableName
    }
    if (model.tableNameChanged) {
        model.tableName = model.tableNameChanged;
        model.tableNameChanged = "";
    }
    path += routeAffix;

    if (model.getParams.length > 0) {
        model.getParams.forEach((param, index) => {
            if (model[param]) {
                if (index === 0) {
                    path += "?";
                }

                path += param + "=" + model[param];
                if (index < model.getParams.length - 1) {
                    path += "&";
                }
            }

        });
    }
    return path;
}


export {urlGenerator};
